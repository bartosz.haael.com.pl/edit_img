#!/usr/bin/python3
#-*- coding:utf-8 -*-



class VectorPY:
	def __init__(self, convert=None, length=None, data=None):
		if (convert is None) and (length is None) and (data is None):
			self.__data = []
		elif (convert is not None) and (length is None) and (data is None):
			self.__data = list(convert)
		elif (convert is None) and (length is not None) and (data is None):
			self.__data = [0] * length
		elif (convert is not None) and (length is not None) and (data is None):
			self.__data = list(convert)
			if length != len(self.__data):
				raise ValueError()
		elif (convert is None) and (length is None) and (data is not None):
			self.__data = data
		elif (convert is not None) and (length is None) and (data is not None):
			self.__data = data
			for n, value in enumerate(convert):
				self.__data[n] = value
		elif (convert is None) and (length is not None) and (data is not None):
			self.__data = data
			if length != len(self.__data):
				raise ValueError()
		elif (convert is not None) and (length is not None) and (data is not None):
			self.__data = data
			for n, value in enumerate(convert):
				self.__data[n] = value
			if length != len(self.__data):
				raise ValueError()
		else:
			raise RuntimeError("Impossible condition.")
	
	def __len__(self):
		return len(self.__data)
	
	def __iter__(self):
		yield from self.__data
	
	def __getitem__(self, index):
		return self.__data[index]
	
	def __setitem__(self, index, value):
		self.__data[index] = value
	
	def __eq__(self, other):
		try:
			if len(self) != len(other):
				return False
		except TypeError:
			return False
		return all((a == b) for (a, b) in zip(self, other))
	
	def __add__(self, other):
		if len(self) != len(other):
			raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
		result = self.__class__(length=len(self))
		for n, (a, b) in enumerate(zip(self, other)):
			result[n] = a + b
		return result
	
	def __radd__(self, other):
		if len(self) != len(other):
			raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
		result = self.__class__(length=len(self))
		for n, (a, b) in enumerate(zip(self, other)):
			result[n] = b + a
		return result
	
	def __iadd__(self, other):
		if len(self) != len(other):
			raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
		for n in range(len(self)):
			self[n] += other[n]
		return self
	
	def __sub__(self, other):
		if len(self) != len(other):
			raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
		result = self.__class__(length=len(self))
		for n, (a, b) in enumerate(zip(self, other)):
			result[n] = a - b
		return result
	
	def __rsub__(self, other):
		if len(self) != len(other):
			raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
		result = self.__class__(length=len(self))
		for n, (a, b) in enumerate(zip(self, other)):
			result[n] = b - a
		return result
	
	def __isub__(self, other):
		if len(self) != len(other):
			raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
		for n in range(len(self)):
			self[n] -= other[n]
		return self
	
	def __mul__(self, other):
		try:
			if len(self) != len(other):
				raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
			result = self.__class__(length=len(self))
			for n, (a, b) in enumerate(zip(self, other)):
				result[n] = a, b
			return result
		except TypeError:
			result = self.__class__(length=len(self))
			for n in range(len(self)):
				result[n] = self[n] * other
			return result
	
	def __imul__(self, other):
		try:
			if len(self) != len(other):
				raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
			result = self.__class__(length=len(self))
			for n in range(len(self)):
				result[n] *= other[n]
			return result
		except TypeError:
			result = self.__class__(length=len(self))
			for n in range(len(self)):
				result[n] *= other
			return result
	
	def __rmul__(self, other):
		try:
			if len(self) != len(other):
				raise ValueError("Lengths of the vectors should be the same. Got {} and {}.".format(len(self), len(other)))
			result = self.__class__(length=len(self))
			for n in range(len(self)):
				result[n] = other[n] * self[n]
			return result
		except TypeError:
			result = self.__class__(length=len(self))
			for n in range(len(self)):
				result[n] = other * self[n]
			return result
	

if __debug__:
	def test_algebra(Vector, Matrix):
		l = ranrange(10)
		for i in range(l):
			c.append(random())
		
		v = Vector(convert=c)
		assert all((_a == _b) for (_a, _b) in zip(v, c))
	
	if __name__ == '__main__':
		test_algebra(Vector, Matrix)

