#!/usr/bin/python3
#-*- coding: utf-8 -*-


import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk
from gi.repository import Gdk as gdk
from gi.repository import GObject as gobject
from gi.repository import GLib as glib

import cairo
import math



class Alphabet(gtk.DrawingArea):
	def __init__(self):
		super().__init__()
		#self.connect('configure-event', self.handle_configure_event)
		self.connect('draw', self.handle_draw)
		
		self.alphabet1 = "aąbcćdeęfghijklłmnńoópqrsśtuvwxyzźż 01234"
		self.alphabet2 = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUVWXYZŹŻ 56789"
		
	def handle_draw(self, drawingarea, context):
		context.set_source_rgb(1, 1, 1)
		context.paint()
		
		rect = self.get_allocation()
		
		context.set_line_width(1)
		context.set_font_size(16)
		context.select_font_face("monospace", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
		
		letter_height = rect.height / len(self.alphabet1)		
		for n, ch in enumerate(self.alphabet1):
			if ch == " ": continue
			context.set_source_rgb(0.5, 0.5, 0.5)
			context.arc(rect.width / 2 - letter_height / 2, letter_height * (n + 0.5), letter_height * 0.45, 0, 2 * math.pi)
			context.stroke()
			context.set_source_rgb(0, 0, 0)
			context.move_to(rect.width / 2 - letter_height / 2 - 6, letter_height * (n + 0.5) + 6)
			context.text_path(ch)
			context.fill_preserve()
			context.stroke()
		
		letter_height = rect.height / len(self.alphabet2)
		for n, ch in enumerate(self.alphabet2):
			if ch == " ": continue
			context.set_source_rgb(0.5, 0.5, 0.5)
			context.arc(rect.width / 2 + letter_height / 2, letter_height * (n + 0.5), letter_height * 0.45, 0, 2 * math.pi)
			context.stroke()
			context.set_source_rgb(0, 0, 0)
			context.move_to(rect.width / 2 + letter_height / 2 - 6, letter_height * (n + 0.5) + 6)
			context.text_path(ch)
			context.fill_preserve()
			context.stroke()


class TileBoard(gtk.DrawingArea):
	def __init__(self, x, y):
		super().__init__()
		
		self.last_mousedown = None
		self.tiles_x = x
		self.tiles_y = y
		self.cursor_position = None, None
		
		self.pointer_t = None
		self.pointer_q = [None, None]
		self.pointer_v = [None, None]
		self.pointer_a = [None, None]
		
		self.pointer_delay = 0.055
		self.factor_q = 1.0
		self.factor_v = 0.0001
		self.factor_a = 0.01
		
		self.connect('configure-event', self.handle_configure_event)
		self.connect('realize', self.handle_realize)
		self.connect('draw', self.handle_draw)
		self.connect('motion-notify-event', self.handle_motion_notify_event)
		self.connect('button-press-event', self.handle_button_press_event)
		self.connect('button-release-event', self.handle_button_release_event)
		
		self.add_events(gdk.EventMask.POINTER_MOTION_MASK)
		self.add_events(gdk.EventMask.BUTTON_RELEASE_MASK)
		self.add_events(gdk.EventMask.BUTTON_PRESS_MASK)
	
	def value(self, i, j):
		return math.tanh(0.08 * ((id(self) + i ^ j) % (min(i, 16, self.tiles_x - i, j, self.tiles_y - j) + 1) - 13 + i % 3 + j % 5))
	
	def handle_configure_event(self, drawingarea, event):
		rect = self.get_allocation()
		self.tile_size = min(rect.width / self.tiles_x, rect.height / self.tiles_y)
		self.rest_x = (rect.width - (self.tiles_x * self.tile_size)) / 2
		self.rest_y = (rect.height - (self.tiles_y * self.tile_size)) / 2
		self.width = rect.width
		self.height = rect.height
	
	def handle_realize(self, drawingarea):
		self.get_window().set_cursor(gdk.Cursor(gdk.CursorType.CROSS))
	
	def handle_draw(self, drawingarea, context):
		tile_size = self.tile_size
		rest_x = self.rest_x
		rest_y = self.rest_y
		width = self.width
		height = self.height
		
		context.set_source_rgb(1, 1, 1)
		context.paint()
		
		clip_x1, clip_y1, clip_x2, clip_y2 = context.clip_extents()
		i_start = max(0, math.floor((clip_x1 - rest_x) / tile_size))
		i_end = min(self.tiles_x, math.ceil((clip_x2 - rest_x) / tile_size))
		j_start = max(0, math.floor((clip_y1 - rest_y) / tile_size))
		j_end = min(self.tiles_y, math.ceil((clip_y2 - rest_y) / tile_size))
		
		for i in range(i_start, i_end):
			x = rest_x + i * tile_size + tile_size / 2
			for j in range(j_start, j_end):
				y = rest_y + j * tile_size + tile_size / 2
				v = self.value(i, j)
				if v > 0:
					context.set_source_rgba(1, 0, 0, min(v, 1))
				elif v < 0:
					context.set_source_rgba(0, 0, 1, min(-v, 1))
				else:
					context.set_source_rgb(1, 1, 1)
				context.rectangle(round(x - tile_size / 2), round(y - tile_size / 2), math.ceil(tile_size), math.ceil(tile_size))
				context.fill()
		
		context.set_line_width(1)
		context.set_source_rgb(0, 0, 0)
		for i in range(i_start, i_end + 1):
			x = round(rest_x + i * tile_size) + 0.5
			context.move_to(x, math.ceil(rest_y))
			context.line_to(x, math.floor(height - rest_y))
			context.stroke()
		for j in range(j_start, j_end + 1):
			y = round(rest_y + j * tile_size) + 0.5
			context.move_to(math.ceil(rest_x), y)
			context.line_to(math.floor(width - rest_x), y)
			context.stroke()
		
		if self.cursor_position[0] != None and self.cursor_position[1] != None:
			i, j = self.cursor_position
			x = rest_x + i * tile_size + tile_size / 2
			y = rest_y + j * tile_size + tile_size / 2
			context.set_source_rgb(0, 0, 0)
			context.rectangle(round(x - tile_size / 2), round(y - tile_size / 2), math.ceil(tile_size), math.ceil(tile_size))
			context.fill()
	
	def handle_motion_notify_event(self, drawingarea, event):		
		t = event.get_time() / 1000
		
		try:
			dt = t - self.pointer_t
		except TypeError:
			dt = 0
		
		q = event.x, event.y
		
		try:
			v = ((q[0] - self.pointer_q[0]) / dt), ((q[1] - self.pointer_q[1]) / dt)
		except (TypeError, ArithmeticError):
			v = 0, 0

		try:
			a = ((v[0] - self.pointer_v[0]) / dt), ((v[1] - self.pointer_v[1]) / dt)
		except (TypeError, ArithmeticError):
			a = 0, 0
		
		self.pointer_q[0] = q[0]
		self.pointer_q[1] = q[1]
		
		self.pointer_v[0] = v[0]
		self.pointer_v[1] = v[1]
		
		self.pointer_t = t
		
		if event.state & (gdk.ModifierType.BUTTON1_MASK | gdk.ModifierType.BUTTON2_MASK | gdk.ModifierType.BUTTON3_MASK | gdk.ModifierType.BUTTON4_MASK | gdk.ModifierType.BUTTON5_MASK) == 0:
			return
		
		#print(self.pointer_q, self.pointer_v, dt)
		
		predicted_x = q[0] + v[0] * self.pointer_delay + a[0] * self.pointer_delay**2 / 2
		predicted_y = q[1] + v[1] * self.pointer_delay + a[1] * self.pointer_delay**2 / 2
		
		#print(predicted_x, predicted_y)
		
		tile_size = self.tile_size
		rest_x = self.rest_x
		rest_y = self.rest_y
		
		if self.cursor_position[0] != None and self.cursor_position[1] != None:
			i, j = self.cursor_position
			x = rest_x + i * tile_size + tile_size / 2
			y = rest_y + j * tile_size + tile_size / 2
			self.queue_draw_area(round(x - tile_size / 2), round(y - tile_size / 2), math.ceil(tile_size), math.ceil(tile_size))
		
		i = math.floor((predicted_x - rest_x) / tile_size)
		j = math.floor((predicted_y - rest_y) / tile_size)
		
		if i < 0 or j < 0 or i >= self.tiles_x or j >= self.tiles_y:
			self.cursor_position = None, None
			if self.get_realized():
				self.get_window().set_cursor(gdk.Cursor(gdk.CursorType.CROSS))
		else:
			self.cursor_position = i, j
			self.get_window().set_cursor(gdk.Cursor(gdk.CursorType.BLANK_CURSOR))
			x = rest_x + i * tile_size + tile_size / 2
			y = rest_y + j * tile_size + tile_size / 2
			self.queue_draw_area(round(x - tile_size / 2), round(y - tile_size / 2), math.ceil(tile_size), math.ceil(tile_size))
	
	def handle_button_press_event(self, drawingarea, event):
		tile_size = self.tile_size
		rest_x = self.rest_x
		rest_y = self.rest_y
		
		if self.cursor_position[0] != None and self.cursor_position[1] != None:
			i, j = self.cursor_position
			x = rest_x + i * tile_size + tile_size / 2
			y = rest_y + j * tile_size + tile_size / 2
			self.queue_draw_area(round(x - tile_size / 2), round(y - tile_size / 2), math.ceil(tile_size), math.ceil(tile_size))
		
		if event.button == gdk.BUTTON_PRIMARY:
			i = math.floor((event.x - rest_x) / tile_size)
			j = math.floor((event.y - rest_y) / tile_size)
			
			if i < 0 or j < 0 or i >= self.tiles_x or j >= self.tiles_y:
				self.cursor_position = None, None
				if self.get_realized():
					self.get_window().set_cursor(gdk.Cursor(gdk.CursorType.CROSS))
			else:
				self.cursor_position = i, j
				self.get_window().set_cursor(gdk.Cursor(gdk.CursorType.BLANK_CURSOR))
				x = rest_x + i * tile_size + tile_size / 2
				y = rest_y + j * tile_size + tile_size / 2
				self.queue_draw_area(round(x - tile_size / 2), round(y - tile_size / 2), math.ceil(tile_size), math.ceil(tile_size))
	
	def handle_button_release_event(self, drawingarea, event):
		tile_size = self.tile_size
		rest_x = self.rest_x
		rest_y = self.rest_y
		
		if self.cursor_position[0] != None and self.cursor_position[1] != None:
			i, j = self.cursor_position
			self.get_window().set_cursor(gdk.Cursor(gdk.CursorType.BLANK_CURSOR))
			x = rest_x + i * tile_size + tile_size / 2
			y = rest_y + j * tile_size + tile_size / 2
			self.queue_draw_area(round(x - tile_size / 2), round(y - tile_size / 2), math.ceil(tile_size), math.ceil(tile_size))
		
		if event.button == gdk.BUTTON_PRIMARY:
			self.get_window().set_cursor(gdk.Cursor(gdk.CursorType.CROSS))
			self.cursor_position = None, None
	
	def reset_after_exception(self):
		pass


if __name__ == '__main__':
	import sys, signal
	
	glib.threads_init()
	
	css = gtk.CssProvider()
	css.load_from_path('style.css')
	gtk.StyleContext.add_provider_for_screen(gdk.Screen.get_default(), css, gtk.STYLE_PROVIDER_PRIORITY_USER)
	
	builder = gtk.Builder()
	builder.add_from_file('interface.glade')
	
	window = builder.get_object('main_window')
	
	box = builder.get_object('top_box')
	
	tileboard1 = TileBoard(100, 100)
	box.pack_start(tileboard1, True, True, 0)
	tileboard2 = TileBoard(100, 100)
	box.pack_start(tileboard2, True, True, 0)
	alphabet = Alphabet()
	alphabet.set_size_request(64, 0)
	box.pack_start(alphabet, False, False, 0)
	
	window.show_all()
	
	mainloop = gobject.MainLoop()
	signal.signal(signal.SIGTERM, lambda signum, frame: mainloop.quit())
	sys.excepthook = lambda *args: (sys.__excepthook__(*args), mainloop.quit())
	window.connect('destroy', lambda window: mainloop.quit())
	
	try:
		mainloop.run()
	except KeyboardInterrupt:
		print()



